package dominio;

public class Prenda{
        private String nombre;
        private String marca;
        private String talla;
	private String stock;

        public void setNombre(String nombre) {
                this.nombre = nombre;
        }

        public String getNombre() {
                return nombre;
        }

        public String getMarca() {
                return marca;
        }

        public void setMarca(String marca) {
                this.marca = marca;
        }

        public void setTalla(String talla) {
                this.talla = talla;
        }

        public String getTalla() {
                return talla;
        }

	public void setStock(String stock) {
		this.stock = stock;
	}

	public String getStock() {
		return stock;
	}

        public Prenda(String nombre, String marca, String talla, String stock) {
                this.nombre = nombre;
                this.marca = marca;
                this.talla = talla;
		this.stock = stock;
	}

	public String generarStringParaMostrar(){
		return ("El/La ") + getNombre() + (" de la marca ") + getMarca() + (" con talla ") + getTalla() + (", le quedan ") + getStock() + (" unidades.");
	}	

        public String toString() {
		return generarStringParaMostrar();
        }
}

